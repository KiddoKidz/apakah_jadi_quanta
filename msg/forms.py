from . import models
from django import forms

class CreateMessageForm(forms.Form):
    submitter = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "fields",
        "required" : True,
        "placeholder" : "Your Name",
        }), max_length=50)

    message = forms.CharField(widget=forms.Textarea(attrs={
        "class" : "fields",
        "required" : True,
        "placeholder" : "Your Message Here",
        }), max_length=50)

class Meta:
    model = models.Message