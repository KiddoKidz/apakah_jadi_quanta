from django.db import models

# Create your models here.
class Message(models.Model):
    submitter = models.CharField(max_length=50)
    msg = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return "{} - {}".format(self.submitter, self.snipped_message())
    
    def snipped_message(self):
        if len(self.msg) > 20 :
            return self.msg[:20] + " ..."
        return self.msg